.. phpMyAdmin documentation master file, created by
   sphinx-quickstart on Wed Sep 26 14:04:48 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de Business 365
=============================================

Contenidos:

.. toctree::
    :maxdepth: 2

    introduccion   
    general 
    seguridad
    contabilidad
    inventario
    facturacion
    compras    
    despachos        
    produccion    
    tareas
    pos
    sistema-pos
    preguntas
    copyright
    glosario   

Indices y tablas
================

* :ref:`genindex`
* :ref:`glosario`
