.. _compras:

Compras
=======


Proveedores
-----------
En proveedores tenemos dos opciones la primera es crear un proveedor o importar un proveedor o varios.

Crear Un proveedor
------------------

**1) Boton:** :guilabel:`Nuevo`.


**2) Tipo Identificacion:** Click en campo correspondiente y desplega la lista de opciones de cedula hasta contraseña.

**3) No.Documento:** Digita el numero de documento del proveedor.

**4) Nombres:** Digita el nombre del proveedor.

**5) Apellidos:** Digita los apellidos del proveedor .

**6) Foto de Perfil:** click en el icono del lapiz carga el logo del proveedor.


**7) Email:** Digita el correo del proveedor @miproveedor.com

**8) Termino de pago:** defina la forma que usted le va pagar al proveedor de contado? 10, 20, 30, .... ,90 dias.

**9) Actividad Economica:** Escriba la activida economica a la cual se dedica  su proveedor.

**10) Observaciones:** Este campo es completa opcional, pero si tienes una obeservavacion no dudes en escribirla aqui, una observacion sobre tu proveedor 

___________________________

Ubicaciones:
------------

**11) Ciudad Ubicación :** La ubicacion donde se encuentra ubicado tu proveedor.

**12) Dirección:** la dirrecion fisica de tu proveedor.

**13) Celular** El numero de celular de tu proveedor.


**14) Telefóno** Telefono fijo de tu proveedor.

**15) Boton** :guilabel:`X` Elimina toda la informacion rellena en ubucaciones.


**17) Boton** :guilabel:`+ Adicionar Ubicacion`. es para agregar mas informacion de ubicaciones del proveedor.

**16) Boton** :guilabel:`Guardar` listo gurada tu informacion.





https://erpv3.source-solutions.co/compras/proveedor/list

Productos
---------

**1) Producto :** :guilabel:`Nuevo`.


**2) Medida:**

**3) Presentación :** .

**4) C. Solicitada:** .

**5) C. Recibida** :guilabel:`Guardar`.

**6) Producto :** :guilabel:`Nuevo`.


**7) Valor Unitario:**

**8) Impuesto :** .

**9) Total:** .

**10) Boton** :guilabel:`X`.


**11) Boton** :guilabel:`+`.

**12) Boton** :guilabel:`Guardar`.


Orden Compras
-------------

**1) Boton:** :guilabel:`Nuevo`.


**2) Surcursal:**

**3) Proveedor:** Digita el nombre del proveedor o su numero de cedula tambien puedes dar click en la lupa para iniciar una busqueda avanzada por filtros.

**4) Fecha de Vencimiento:** Es precio que va tener el producto al cliente final.

**5) Observaciones:** :guilabel:`Guardar`.

Productos
---------

**1) Producto :** :guilabel:`Nuevo`.


**2) Medida:**

**3) Presentación :** .

**4) C. Solicitada:** .

**5) C. Recibida** :guilabel:`Guardar`.

**6) Producto :** :guilabel:`Nuevo`.


**7) Valor Unitario:**

**8) Impuesto :** .

**9) Total:** .

**10) Boton** :guilabel:`X`.


**11) Boton** :guilabel:`+`.

**12) Boton** :guilabel:`Guardar`.





.. Hint:: https://erpv3.source-solutions.co/compras/orden-compra/list.


Creación
++++++++


Recepción
+++++++++


