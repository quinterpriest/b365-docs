.. _sistema_pos:

Sistema POS
===========

Un sistema POS de facturación o punto de venta en español, es un conjunto de herramientas de hardware y software, 
que principalmente le permiten a los negocios facturar sus ventas, facilitando también llevar el control de su flujo de caja, inventarios, proveedores, compras,  cuentas por cobrar y pagar, gastos y costos fijos, utilidades y pérdidas, entre otras funciones.

.. _sistema_pos_login:

Logueo
------

Para acceder al sistema POS, se debe ingresar el siguiente :term:`URL` dentro de su :term:`Navegador web` Google Chrome:
https://erpv3.source-solutions.co/pos
Nota: Para efectos de este tutorial se ingresará a la aplicación utilizando un entorno de pruebas o
“demostración”.

.. image:: images/sistema-pos/pantalla-login.png
  :width: 550
  :align: center
  :alt: Sistema Pos login

Una vez digitado el :term:`URL` correspondiente, se desplegará la página de inicio del sistema :term:`POS`, donde se
podrá ingresar el número de documento y contraseña asignados.

Luego de ingresar el número de documento y contraseña, se da clic en el botón "Ingresar" y se mostrará la pantalla inicial para empezar a trabajar con el sistema :term:`POS`. 

Consulta apertura de turno :ref:`sistema_pos_apertura_turno`


.. _sistema_pos_apertura_turno:

Apertura de turno
-----------------

Cuando se ingresa exitosamente al sistema :term:`POS`, se desplegará una ventana con un formulario para realizar la apertura del turno, en este formulario se debe ingresar:

* Monto Inicial: Base de dinero en efectivo con el que se inicia el turno.
* Observaciones (Opcional): Aclaración acerca de la cantidad digita en el campo "Monto Inicial".

Por último se da click en el botón “Aceptar” y si el proceso fue exitoso el sistema mostrará un mensaje que dice “Se abrió el turno correctamente”.

.. image:: images/sistema-pos/apertura-turno.png
  :width: 550
  :align: center
  :alt: Sistema Pos apertura turno

.. _sistema_pos_busqueda_producto:

Búsqueda de un producto
-----------------------

Para registrar un producto en una orden, este se puede buscar, utilizando algunas de las opciones
que se mencionan a continuación:

.. _sistema_pos_busqueda_general:

Búsqueda General
++++++++++++++++

Puede buscar un producto del siguiente listado, identificando por medio de la imagen, el que desea
añadir a la orden. Una vez identificado el producto, lo debe agregar a la orden dando un clic sobre el
producto.

.. _sistema_pos_busqueda_categoria:

Búsqueda por categoría
++++++++++++++++++++++

Si usted da click en una categoría, en el listado general le saldrán únicamente los productos
correspondientes a esa categoría, eso le permite ubicar fácilmente un producto y registrarlo en una
orden. Una vez identificado el producto, lo debe agregar a la orden dando un clic sobre el producto.

.. _sistema_pos_busqueda_nombre:

Búsqueda por nombre
+++++++++++++++++++

Para buscar un producto por el nombre, debe ingresar en el campo “Nombre de Producto”, una
palabra relevante del nombre del producto, para que el resultado de la búsqueda sea efectivo. Por
ejemplo, si vamos a buscar “Perro Americano” y sabemos que tenemos más productos creados como
“Perro”, la idea es buscar con la palabra “Americano”. Tenga en cuenta que este campo es de
autocompletar, eso quiere decir, que no es necesario escribir toda la palabra que desea buscar. Una
vez identificado el producto, lo debe agregar a la orden dando un clic sobre el producto.

.. _sistema_pos_busqueda_barras:

Búsqueda por Código de Barras
+++++++++++++++++++++++++++++


.. _sistema_pos_busqueda_referencia:

Búsqueda por Referencia
+++++++++++++++++++++++

Para buscar un producto por referencia, usted puede ingresar la referencia en el campo “Referencia
Producto” y oprimir la tecla Enter del teclado de su computador, esto agrega automáticamente el
producto a la orden del cliente.

.. image:: images/sistema-pos/busqueda-referencia.png
  :width: 550
  :align: center
  :alt: Sistema Pos Busqueda Referencia

.. _sistema_pos_tipo_venta:

Tipo de Venta
-------------

Para registrar un producto en una orden, este se puede buscar, utilizando algunas de las opciones
que se mencionan a continuación:

.. image:: images/sistema-pos/tipo-venta.png
      :width: 550
      :align: center
      :alt: Sistema Pos Tipo Venta

.. _sistema_pos_venta_mesa:

Venta en Mesa
+++++++++++++

Para realizar una venta en una mesa, se deben realizar los siguientes pasos:

#. Seleccionar la opción “Mesa”.
#. Seleccionar una mesa. Este selección se puede realizar de dos maneras:

  * Utilizando el selector de mesas. 

    .. image:: images/sistema-pos/selector-mesas.png
      :width: 450
      :align: center
      :alt: Sistema Pos Selector Mesas

  * Dando clic en el icono de mesas ocupadas, que se encuentra en la parte superior derecha de la pantalla, donde se podrá ver en color verde las mesas disponibles y en rojo las mesas ocupadas, de esta manera damos clic sobre la palabra “Seleccionar” dentro de la mesa que se vaya a asignar a la orden del cliente.

    .. image:: images/sistema-pos/barra-superior-mesas-ocupadas.png
      :width: 450
      :align: center
      :alt: Sistema Pos Selector Mesas

    .. image:: images/sistema-pos/mesas-ocupadas.png
      :width: 450
      :align: center
      :alt: Sistema Pos Mesas Ocupadas

  * Seleccionar o crear un cliente. Ver Creación de Clientes :ref:`sistema_pos_crear_cliente`
  
    Nota: Este paso es opcional, eso quiere decir, que se puede crear la orden sin necesidad de agregarle un Cliente. Este se debe agregar si el cliente solicita que la factura salga a nombre de él.
  
  * Seleccionar un mesero: Para esto se debe digitar el nombre del mesero, en el campo “Mesero”, que se quiere asignar a la orden. Tenga en cuenta que este campo es de autocompletar, eso quiere decir, que no es necesario escribir todo el nombre del mesero qué desea buscar. Una vez identificado el Mesero, lo debe agregar a la orden dando un clic sobre el nombre del Mesero.

    .. image:: images/sistema-pos/seleccionar-mesero.png
      :width: 450
      :align: center
      :alt: Sistema Pos Seleccionar meseros
  
    Nota: Si desea agregar un mesero y cuenta con los permisos adecuados, debe crearlo antes por el sistema administrativo y actualizar el sistema POS o comunicarse con el administrador del sistema para que este lo cree.
  
  * Seleccionar los productos que se quieren agregar a la orden. Ver Búsqueda de Productos :ref:`sistema_pos_busqueda_producto`


.. _sistema_pos_venta_directa:

Venta Directa
+++++++++++++

Una venta directa o venta rápida se utiliza para facturar aquellos productos que el cliente pide pero no
los consume en el establecimiento, con el fin de no ocupar una mesa, o por ejemplo, cuando pide
solamente una o dos bebidas. Para realizar una venta directa, se deben realizar los siguientes pasos:
1. Seleccionar la opción “Directo”.
2. Seleccionar o crear un cliente. Ver Creación de Clientes.
Nota: Este paso es opcional, eso quiere decir, que se puede crear la orden sin necesidad de agregarle
un Cliente. Este se debe agregar si el cliente solicita que la factura salga a nombre de él.
3. Seleccionar un mesero: Para esto se debe digitar el nombre del mesero, en el campo
“Mesero”, que se quiere asignar a la orden. Tenga en cuenta que este campo es de
autocompletar, eso quiere decir, que no es necesario escribir todo el nombre del mesero qué
desea buscar. Una vez identificado el Mesero, lo debe agregar a la orden dando un clic sobre
el nombre del Mesero.
Nota: Si desea agregar un mesero y cuenta con los permisos adecuados, debe crearlo antes por el
sistema administrativo y actualizar el sistema POS o comunicarse con el administrador del sistema para
que este lo cree.
4. Seleccionar los productos que se quieren agregar a la orden. Ver Búsqueda de Productos.


.. _sistema_pos_venta_domicilio:

Venta de Domicilio
++++++++++++++++++

Para realizar una venta de domicilio, se deben realizar los siguientes pasos:
1. Seleccionar la opción “Domicilios”.
2. Seleccionar una plataforma de Domicilio: Se selecciona “Rappi”, cuando se vaya a facturar
una orden de Rappi o se seleccionar “Domicilios”, cuando se vaya a facturar una orden de
Domicilios.com, y así para las demás opciones. Para los domicilios de Mr.Bross no se debe
seleccionar ninguna opción.
3. Seleccionar o crear un cliente. Ver Creación de Clientes.
Nota: Para los domicilios si es necesario ingresar los datos del cliente.
4. Seleccionar un domiciliario: Para esto se debe digitar el nombre del domiciliario, en el campo
“Domiciliario”, que se quiere asignar a la orden. Tenga en cuenta que este campo es de
autocompletar, eso quiere decir, que no es necesario escribir todo el nombre del domiciliario
que desea buscar. Una vez identificado el Domiciliario, lo debe agregar a la orden dando un
clic sobre el nombre del Domiciliario.
Nota: Si desea agregar un domiciliario y cuenta con los permisos adecuados, debe crearlo antes por el
sistema administrativo y actualizar el sistema POS o comunicarse con el administrador del sistema para
que este lo cree.
5. Seleccionar los productos que se quieren agregar a la orden. Ver Búsqueda de Productos.

.. _sistema_pos_crear_cliente:

Creación de clientes
--------------------

La selección y creación de clientes aplica para las ventas en mesa, directas y domicilios.

Crear un cliente
++++++++++++++++
Para crear un Cliente, se deben realizar los siguientes pasos:
1. De clic sobre el botón “Crear Cliente” , que se encuentra en el campo “Cliente”.
2. Luego se despliega una ventana con un formulario, donde se deben ingresar los siguientes
datos:
- Nombres (Campo Obligatorio): Se debe ingresar el nombre del cliente o de la empresa.
- Apellidos (Campo Obligatorio): Se debe ingresar el apellido del cliente o en caso de la
empresa, si esta es: SA, SAS u otra.
- Número de Documento: Se ingresa el número de documento para el cliente o nit en
caso de una empresa.
- Celular (Campo Obligatorio): Se debe ingresar número telefónico del cliente o
empresa.
- Email: Se ingresa el email para el cliente o de la empresa.
- Fecha de Nacimiento: Se ingresa la fecha de nacimiento del cliente.
- Dirección (Campo Obligatorio): Se debe ingresar la dirección de domicilio de la
persona o de la empresa. Se pueden agregar tantas direcciones maneje el cliente, esto se hace dando 
clic en botón “Agregar Dirección” y se debe marcar, con un check ,
la dirección por defecto del cliente. Si se quiere eliminar alguna dirección se utiliza el
botón “Eliminar” .
Nota: Los campos obligatorios siempre deben de ir diligenciados, de lo contrario el sistema no permitirá
crear el cliente. En caso de que no tenga todos los datos que el sistema le pida como obligatorios,
ingrese un punto(.). Se recomienda solicitar los datos obligatorios al cliente.
3. Dar clic en el botón “Aceptar”, para que guarde la información, se cierre el formulario y
continúe con la creación de la orden.
4. En caso de que no desee crear el cliente, de clic sobre el botón “Cancelar”. Esto cerrará el
formulario, no se guardará la información y podrá continuar con la creación de la orden.

Seleccionar un cliente
++++++++++++++++++++++

Para seleccionar un cliente, se deben realizar los siguientes pasos:
1. Se debe digitar el nombre del cliente, en el campo “Cliente”, que se quiere asignar a la orden.
Tenga en cuenta que este campo es de autocompletar, eso quiere decir, que no es necesario
escribir todo el nombre del Cliente qué desea buscar. Una vez identificado el Cliente, lo debe
agregar a la orden dando un clic sobre el nombre del Cliente.
Si al momento de digitar el nombre de cliente, no le muestra ninguna opción, entonces lo debe
crear. Ver Crear un Cliente.
2. Luego se despliega una ventana con un formulario, cuya funcionar es la de actualizar, si es el
caso, los datos del cliente:
- Nombres (Campo Obligatorio): Se debe ingresar el nombre del cliente o de la empresa.
- Apellidos (Campo Obligatorio): Se debe ingresar el apellido del cliente o en caso de la
empresa, si esta es SA, SAS u otra.
- Número de Documento: Se ingresa el número de documento para el cliente o nit en
caso de una empresa.
- Celular (Campo Obligatorio): Se debe ingresar número telefónico del cliente o
empresa.
- Email: Se ingresa el email para el cliente o de la empresa.
- Fecha de Nacimiento: Se ingresa la fecha de nacimiento del cliente.
- Dirección (Campo Obligatorio): Se debe ingresar la dirección de domicilio de la
persona o de la empresa.
Se pueden agregar tantas direcciones maneje el cliente, esto se hace dando clic en
botón “Agregar Dirección” y marcando con un check , la dirección por defecto del
cliente. Si se quiere eliminar alguna dirección se utiliza el botón “Eliminar” .
Nota: Los campos obligatorios siempre deben de ir diligenciados, de lo contrario el sistema no permitirá
crear el cliente. En caso de que no tenga todos los datos que el sistema le pida como obligatorios,
ingrese un punto(.). Se recomienda solicitar los datos obligatorios al cliente.

3. Dar clic en el botón “Aceptar”, para que actualice la información, se cierre el formulario y
continúe con la creación de la orden.



Creación de una orden
---------------------

Para crear una orden, es importante haber realizado los pasos del 1 al 6 de este tutorial.
Adicionalmente, para guardar la orden se puede hacer de 3 formas:
1. Utilizando el botón “Imprimir Orden”: (Recomendada) Cuando se oprime este botón, se
imprime automáticamente 1 copia de la orden para la cocina (Depende de la configuración del
sistema, se pueden imprimir 2 copias), posteriormente, se guarda automáticamente la orden y
el sistema limpia los registros digitados para empezar a digitar una nueva orden.

2. Utilizando el botón “Guardar Orden”: Cuando se oprime este botón, se guarda
automáticamente la orden y el sistema limpia los registros digitados para empezar a digitar
una nueva orden. Esta opción se utiliza mayormente cuando se edita una orden.
3. Pagar o cerrar una orden. Ver Pagar una Orden y Creación de Factura de Venta.

Buscar una orden abierta
------------------------

Buscar una Orden de Mesa
++++++++++++++++++++++++

Para buscar una orden de mesa, se debe ubicar el icono de mesas disponibles, que se encuentra en
la parte superior derecha de la pantalla, este icono muestra dentro un círculo rojo, un indicador de las
mesas que están actualmente ocupadas o en otras palabras, nos muestra las órdenes actualmente
abiertas .
Cuando se accede a la pantalla de mesas disponibles, podremos visualizar en color rojo las mesas
ocupadas u órdenes actualmente abiertas, con las cuales podremos realizar las siguientes acciones:
1. Reimprimir una orden: Dentro de la mesa ocupada, ubicamos un texto que dice “Orden”,
damos clic sobre él y automáticamente se imprime una copia de la orden de cocina.
2. Editar una orden: Dentro de la mesa ocupada, ubicamos un texto que dice “Editar”, damos clic
sobre él y automáticamente los datos inicialmente ingresados en la orden se mostrarán para
que se pueden modificar, para más detalle, Ver Modificación de una Orden.

Buscar una Orden Directa
++++++++++++++++++++++++

Para buscar una orden directa, se debe ubicar el icono de ventas directas, que se encuentra en la
parte superior derecha de la pantalla , este icono muestra dentro un círculo rojo, un indicador
de las ventas u órdenes directas que están actualmente abiertas.

Cuando se accede a la pantalla de ventas directas, podremos visualizar por medio de un listado, las
órdenes directas que actualmente se encuentran abiertas.
Una orden abierta la podemos buscar fácilmente, por medio del número de orden, de la siguiente
manera:
1. Digitando en el campo “Referencia”, el número de orden correspondiente, este número se
puede ver en la impresión de la orden de cocina.
2. Luego de da clic en el botón “Buscar” y el sistema, solo mostrará el registro correspondiente al
número de orden digitado, sino lo muestra es por que la orden ya debe estar pagada o
cerrada.
Por cada orden directa abierta podremos realizar las siguientes acciones:
3. Reimprimir una orden: Dentro de la orden abierta, ubicamos un texto que dice “Orden”, damos
clic sobre él y automáticamente se imprime una copia de la orden de cocina.
4. Editar una orden: Dentro de la orden abierta, ubicamos un texto que dice “Editar”, damos clic
sobre él y automáticamente los datos inicialmente ingresados en la orden se mostrarán para
que se pueden modificar, para más detalle, Ver Modificación de una Orden.
Si desea volver a mostrar todas la órdenes directas abiertas, debe dar clic en el botón “Limpiar”.

Buscar una Orden de Domicilio
+++++++++++++++++++++++++++++

Para buscar una orden de domicilio, se debe ubicar el icono de domicilios, que se encuentra en la
parte superior derecha de la pantalla , este icono muestra dentro un círculo rojo, un indicador
de las ventas u órdenes de domicilio que están actualmente abiertas.
Cuando se accede a la pantalla de ventas de domicilio, podremos visualizar por medio de un listado
las órdenes de domicilio que actualmente se encuentran abiertas.
Una orden abierta la podemos buscar fácilmente, por medio del número de orden, de la siguiente
manera:
1. Digitando en el campo “Referencia”, el número de orden correspondiente, este número se
puede ver en la impresión de la orden de cocina.
2. Luego de da clic en el botón “Buscar” y el sistema, solo mostrará el registro correspondiente al
número de orden digitado, sino lo muestra es por que la orden ya debe estar pagada o
cerrada.
Por cada orden directa abierta podremos realizar las siguientes acciones:
3. Reimprimir una orden: Dentro de la orden abierta, ubicamos un texto que dice “Orden”, damos
clic sobre él y automáticamente se imprime una copia de la orden de cocina.
4. Editar una orden: Dentro de la orden abierta, ubicamos un texto que dice “Editar”, damos clic
sobre él y automáticamente los datos inicialmente ingresados en la orden se mostrarán para
que se pueden modificar, para más detalle, Ver Modificación de una Orden.
Si desea volver a mostrar todas la órdenes de domicilio abiertas, debe dar clic en el botón “Limpiar”.

Modificación de una Orden
+++++++++++++++++++++++++

Para modificar una orden abierta, busquela por la pantalla correspondiente de acuerdo a la venta que
está realizando (Ver Buscar una Orden Abierta), ubique el botón “Editar” y de clic sobre él,
automáticamente la información de la orden se mostrará para que se puedan agregar más productos
o pagar la orden (Ver Pagar una Orden y Creación de Factura de Venta).


Pagar una orden y crear factura de venta
----------------------------------------

Para pagar o cerrar una orden abierta, de clic sobre el botón “Pagar”, esto desplegará una ventana
donde podrá agregar propinas y realizar el pago de la orden.

Propinas
++++++++

Nota: La opción de propinas es totalmente configurable, esto quiere decir, es que es potestad del administrador
del sistema si habilita o no esta opción dentro del sistema. Para efectos de este tutorial, se simulará que la
opción de propinas se encuentra habilitada.
Cuando se ingresa a la ventana donde podremos realizar los pagos de la orden, en la parte superior
de esta, aparecerá la sección de propinas, siempre por defecto deshabilitada. Una vez se habilite la
sección de propinas con el botón (Cuando se habilita la propina el botón queda así ),
el valor de propinas que aparece en el campo propina se sumará automáticamente al valor total de la
venta. El valor de la propina que aparece, corresponde al 10% de la venta total.
El valor de la propina se puede modificar, borrando el dato que muestra por defecto y escribiendo el
nuevo valor.
Si deshabilita de nuevo la opción de propinas, al valor total de la venta le será restado el valor de la
propina.

Pago Normal
+++++++++++
Cuando se ingresa a la ventana donde podremos realizar los pagos de la orden, el sistema por
defecto siempre mostrará un pago con la forma de pago en “Efectivo” y por el valor total de la venta.
Usted podrá modificar la forma de pago dando clic sobre el campo forma de pago y seleccionando
otra opción.

Pago Mixto
++++++++++
Cuando se ingresa a la ventana donde podremos realizar los pagos de la orden, el sistema por
defecto siempre mostrará un pago con la forma de pago en “Efectivo” y por el valor total de la venta.
Si le van a pagar la orden con diferentes formas de pago o en otras palabras, desea realizar un pago
Mixto, usted podrá modificar la forma de pago dando clic sobre el campo forma de pago,
seleccionando otra opción y modificando por un menor valor, el dato que le aparezca en el campo
“Dinero Recibido”. Cuando se digita un menor valor, el sistema muestra el saldo pendiente por pagar
y un botón con nombre “Agregar Pago”. Si se da clic en este botón, automáticamente se mostrará
otro pago por el saldo de la factura y con forma de pago en “Efectivo”, así lo que restaría sería
modificar el nuevo pago respecto a cómo le vayan a pagar la orden. Se podrán agregar tantos pagos
sean necesarios o como le vayan a pagar la orden.

Para los Pagos Normales o Mixtos, tenga en cuenta que cuando selecciona las opciones de
“Tarjetas” en la forma de pago, esto le despliega los campos de Franquicia (Corresponde a la marca
de la tarjeta: Visa, MasterCard, Diners, etc) y Referencia (Es el número de autorización que aparece
en el boucher), los cuales recomendamos que sean diligenciados

Creación de una Factura de Venta
++++++++++++++++++++++++++++++++

Una vez se hayan hecho los pagos correspondientes, para cerrar la orden o crear la factura de venta,
debe dar clic sobre el botón “Aceptar” (Si no desea realizar el pago de la orden, debe dar clic en el
botón “Cancelar”), esto permitirá imprimir 1 copia de la factura de venta cerrada (Depende de la
configuración del sistema, se pueden imprimir 2 copias) y aparecerán otros botones donde podrá
Reimprimir 1 copia de la factura de venta y/o 1 copia de la orden de cocina.
Para finalizar el proceso de pago y empezar una nueva factura, le doy clic en el botón Cerrar”.


Buscar una orden cerrada o factura de venta
-------------------------------------------

Para buscar una orden cerrada o factura de venta, se debe ubicar el texto “Menú”, que se encuentra
en la parte superior izquierda de la pantalla . Cuando se accede a la pantalla del menú,
ubicamos la opción que dice “Ordenes” y accedemos a ella dando clic.
En la pantalla de “Ordenes” podremos apreciar tanto órdenes abiertas (Aquí también aplican los
pasos explicados en Buscar una Orden Abierta), como órdenes cerradas, pero en esta ocasión nos
centraremos en estas últimas.
Una orden cerrada la podemos buscar fácilmente, por medio del número de orden:

1. Digitando en el campo “Referencia”, el número de orden correspondiente, este número se
puede ver en la impresión de la orden de cocina o en la impresión de la factura de venta
cerrada.
O del consecutivo de la factura:
2. Digitando en el campo “Consecutivo Factura”, el número de consecutivo de factura
correspondiente, este número se puede ver en la impresión de la factura de venta.
Para ambos casos, se debe dar clic en el botón “Buscar” y el sistema mostrará el registro
correspondiente al número de orden o consecutivo de factura digitado.
Por cada orden cerrada, podremos realizar las siguientes acciones:

Reimprimir la Orden de Cocina(aplica restaurantes)
++++++++++++++++++++++++++++++++++++++++++++++++++

Dentro de la orden cerrada, ubicamos un texto que dice “Orden”, damos clic sobre él y
automáticamente se imprime una copia de la orden de cocina.

Reimprimir la Factura de Venta
++++++++++++++++++++++++++++++

Dentro de la orden cerrada, ubicamos un texto que dice “Factura”, damos clic sobre él y
automáticamente se imprime una copia de la factura de venta.
Si desea volver a mostrar todas la órdenes de domicilio abiertas, debe dar clic en el botón “Limpiar”.

Editar valores y formas de pago
+++++++++++++++++++++++++++++++

Dentro de la orden cerrada, ubicamos un texto que dice “Editar”, damos clic sobre él y
automáticamente se despliega un formulario con los pagos ingresados al momento de cerrar la orden.
El proceso de modificación de valores y formas de pagos es similar al proceso de pagar un orden.
Ver Pago Normal y Pago Mixto en Pagar una orden y creación de factura de venta. Una vez se
realice la modificación o creación de pagos, se deben realizar las siguientes acciones:
1. Dar clic en el botón “Actualizar Orden”: Esto actualiza la factura de venta con los pagos
ingresados.
2. Reimprimir la factura de venta: Se recomienda imprimir de nuevo la factura para que quede
evidencia del cambio de las formas de pago y los valores.
3. Dar clic en el botón “Cerrar”: Cierra el formulario y carga automáticamente la pantalla para
crear una nueva orden.


Impresión de reportes
---------------------

Para la impresión de reportes, debe ubicar el texto “Menú”, que se encuentra en la parte superior
izquierda de la pantalla . Cuando se accede a la pantalla del menú, ubicamos la opción
que dice “Turnos” y accedemos a ella dando clic.
En la pantalla de “Turnos” podremos apreciar un listado con todos los turnos realizados hasta el
momento. Por cada turno podremos realizar las siguientes acciones:

Impresión de Reporte Fiscal
+++++++++++++++++++++++++++

Dentro del turno, ubicamos un texto que dice “Fiscal”, damos clic sobre él y automáticamente se
imprime el reporte fiscal. Este es un resumen de las facturas generadas y los valores de ventas en el
turno.

Impresión de Reporte de Meseros
+++++++++++++++++++++++++++++++

Dentro del turno, ubicamos un texto que dice “Meseros”, damos clic sobre él y automáticamente se
imprime el reporte de meseros. Este es un resumen de las ventas realizadas por cada mesero.

Impresión de Reporte de Domicilios
++++++++++++++++++++++++++++++++++

Dentro del turno, ubicamos un texto que dice “Domicilios”, damos clic sobre él y automáticamente se
imprime el reporte de domicilios. Este es un resumen de las ventas realizadas por cada domicilio.

Cierre de turno
---------------

Para realizar el cierre del turno, debe ubicar el texto “Menú”, que se encuentra en la parte superior
izquierda de la pantalla . Cuando se accede a la pantalla del menú, ubicamos la opción
que dice “Cerrar Caja” y accedemos a ella dando clic.
Posteriormente se desplegará un formulario donde se muestran los valores registrados a través del
sistema por cada medio de pago. Tenga en cuenta que si existen ordenes abiertas, el sistema le
informará al respecto antes de cerrar el turno, en ese caso, debe verificar que esas ordenes se deben
o no facturar, en caso de que no, cierre el turno e informe al administrador del sistema.

Así mismo, habrá un campo de texto con nombre “Valor Contado”, donde deberá ingresar los valores
que usted tenga físicamente por cada medio de pago (Tenga en cuenta que el monto inicial también
debe registrarse, le recomendamos que ese valor sea sumado al valor del efectivo). Vale la pena
indicar que los valores que muestra el sistema y los que usted digite deben coincidir y arrojar
diferencia en cero (0). De lo contrario le recomendamos que haga un reconteo los valores que usted
tenga físicamente. Por último, de forma opcional, ingrese un texto aclaratorio sobre algún evento
presentado durante su turno en el campo “Observaciones”.

Una vez digitado los datos de cierre de turno y haber validado que la información es la correcta, de
clic sobre el botón “Cerrar Turno”, para finalizar el proceso y si desea imprimir el cierre de turno, de
clic sobre el botón “Imprimir Turno”.

Salir del sistema
-----------------

Para salir del sistema, debe ubicar el texto “Salir”, que se encuentra en la parte superior izquierda de
la pantalla