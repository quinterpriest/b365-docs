.. _preguntas:

Preguntas
=========

Conoce la sección con las preguntas y respuestas más consultadas de nuestro servicios.

.. _faqpagos:

Pagos
++++++

.. _faq1_1:

Cómo pagar en línea
-------------------

Try to set the :config:option:`$cfg['OBGzip']`  directive to ``false`` in your
:file:`config.inc.php` file and the ``zlib.output_compression`` directive to
``Off`` in your php configuration file.

.. _faq1_2:

1.2 My Apache server crashes when using phpMyAdmin.
---------------------------------------------------

You should first try the latest versions of Apache (and possibly MySQL). If
your server keeps crashing, please ask for help in the various Apache support
groups.

