.. _copyright:

Copyright
=========

.. code-block:: none

    Copyright (C) 2019 Edgar Ramos <edgar.ramos@source-solutions.co>
        Geyffer Acosta <geyffer.acosta@source-solutions.co>
        Jefferson Quintero <jefferson.quintero@source-solutions.co>
        [consultar créditos para más detalles]
