.. _general:

Modulo General
===============
 Desde este modulo pudes agregar los nuevos consecutivos y configurarlos, agregar, eliminar o editar resgistros de personas o empleados. 
Consecutivos y otras configuraciones
------------------------------------


Que le sigue o le sucede a otro sin interrupción
Estos son un elemento de numeración clave en la :guilabel:`facturacion`, con esto saberemos hasta que numero podemos facturar o en qué fecha se vence nuestra facturación, 

Agregar dos fotos dos una del pos y otra de un ejemplo de esta información de Dian con fin de poder ilustrar un poco más como hacer el proceso

Crear un Consecutivo
--------------------

Paso a Paso

*1 Boton :guilabel:`Nuevo`.

*2 campo _Sucursal_ dale click en el campo se abre una lista desplegable con las surcursables diponibles elegi una (*Obligatorio).

*3 Campo _Nombre_ escriba un nombre para su consecutivo(*Obligatorio).

*4 Campo _Sufijo_ es totalmente opcional, este no es (*Obligatorio) .

*5 Campo de Numero_ Inicial_  el numero inicial es donde  inicia la facuracion a medida que se factura mas este numero debe actualizar (*Obligatorio).

*6 Campo Numero_ Actual_ en este campo se debe poner numero actual de facturacion.

*7 Valido Hasta aqui debemos elegir fecha esta cuando estara valido el consecutivo, dale click en cuadrito azul con icono de calendario.

*8 Descripcion es opcional .

*9 Prefijo este numero estara al final del numero de facturacion

*10 Numero de digitos es simplemente la candida de ceros que tendra al comienzo factura en su numero, ejemplo 1 significa un 0 al comienzo del numero de factura, 5 seria igual a 00000 al comienzo de la factura.

*11 Numero final sera ultimo numero con el cual se podra facturar con este consecutivo.

*12 Valido Desde es la fecha de activacion del consecutivo osea en el momento que se esta creando.

*13 Activo (casilla de verificacion o checkbox)
solo un click en el cuadrito para tener el consecutivo.

*8 por ultimo para guardar el consecutivo click en el boton :guilabel:`Guardar`



    .. seealso::

	https://erpv3.source-solutions.co/general/consecutivo/list  
Ruta::file:`General> Configuracion> Consecutivos`



Modulo de Personas
------------------

Descripcion
------------------

Este módulo es muy esencial básicamente para toda gestionar toda información relacionada con personas debes agregarlas primero aquí, empleados, clientes.


Crear Persona
------------------
*1 Boton :guilabel:`Nuevo`.

*2 Tipo de identificacion_ seleccione un tipo de la lista desplegable valido en el caso espeficifico del resgistro (*Obligatorio).

*3 No.Documento_ digite el numero de docummento de la persona que esta registrando (*Obligatorio).

*4 Nombres_ digite nombre de la persona que va registrar.

*5 Apellidos_ digite los apellidos o el apellido de la persona que va registrar.

*6 Campo Email_ aqui debe digitar el correo electronico de la persona que va registrar. 

*7 Ciudad Nacimiento aqui debe digitar la ciudad de nacimiento de la persona que va registrar, o dale click en la lupa para hacer una busqueda avanzada  con varios tipos de filtros como departamento,codigo Dane entre otros, para iniciar la busqueda click en  :guilabel:`Buscar`. una vez encontrado el nombre de la ciudad que necesitas click en la casilla con el chulo cierra la ventana  click en la :guilabel:`x`.


*8 Estado Civil escoje de la lista desplegable entre soltero o casado es opcional.

Informacion de Contacto
-----------------------
La informacion es esencial para contactar a la persona. 
*9 Ciudad Ubicacion: en este campo de auto completar puedes iniciar escribir el nombre de la ciudad y te lanzara las sugerencias en una lista desplegable (*Obligatorio).

*10 Dirrecion: Digita la dirrecion (*Obligatorio)

*11 Celular: Digita el numero de celular.

*12 Telefono: Digita el numero telefonico.

*13 :guilabel:`+ Adiccionar Ubicacion:`. en este boton puedes agregar mas datos de la misma persona en caso que tenga mas de una direccion etc..

*14 Por ultimo click en el boton :guilabel:`Guardar`.
.. seealso::

	http://erpv3.source-solutions.co/general/persona/list
Ruta::file:`General> Configuracion> Cons

Busqueda de Persona
-------------------

Empleados
---------------------

Descrpcion
----------
podemos agregar, modificar y eliminar de la lista a los empleados.

1 Boton :guilabel:`Nuevo`.

*2 Tipo de identificacion_ seleccione un tipo de la lista desplegable valido en el caso espeficifico del resgistro (*Obligatorio).

*3 No.Documento_ digite el numero de docummento de la persona que esta registrando (*Obligatorio).

*4 Nombres_ digite nombre de la persona que va registrar.

*5 Apellidos_ digite los apellidos o el apellido de la persona que va registrar.

*6 Campo Email_ aqui debe digitar el correo electronico de la persona que va registrar. 

*7 Ciudad Nacimiento aqui debe digitar la ciudad de nacimiento de la persona que va registrar, o dale click en la lupa para hacer una busqueda avanzada  con varios tipos de filtros como departamento,codigo Dane entre otros, para iniciar la busqueda click en  :guilabel:`Buscar`. una vez encontrado el nombre de la ciudad que necesitas click en la casilla con el chulo cierra la ventana  click en la :guilabel:`x`.


*8 Estado Civil escoje de la lista desplegable entre soltero o casado es opcional.

Informacion de Contacto
-----------------------
La informacion es esencial para contactar a la persona. 
*9 Ciudad Ubicacion: en este campo de auto completar puedes iniciar escribir el nombre de la ciudad y te lanzara las sugerencias en una lista desplegable (*Obligatorio).

*10 Dirrecion: Digita la dirrecion (*Obligatorio)

*11 Celular: Digita el numero de celular.

*12 Telefono: Digita el numero telefonico.

*13 :guilabel:`+ Adiccionar Ubicacion:`. en este boton puedes agregar mas datos de la misma persona en caso que tenga mas de una direccion etc..

*14 Por ultimo click en el boton :guilabel:`Guardar`.


    .. seealso::

	https://erpv3.source-solutions.co/general/empleado/list 
Ruta::file:`General> Configuracion> Consecutivos`

Busqueda Empleados
------------------