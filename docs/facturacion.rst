.. index:: config.inc.php

.. _facturacion:

Facturación
===========

Es el documento físico que constata de la operación de compra y venta de un bien o 
Servicio, la cual cuenta con toda la información de la transacción con datos como fecha y lugar, datos del vendedor, precios y IVA.
Por medio de la facturación es posible las reclamaciones, garantías y verificar si cliente efectivamente nos compró un producto. 

En Colombia solo existen 4 tipos de Métodos para la facturación avalados por la DIAN
Dirección de Impuestos y  Aduanas Nacionales

Nosotros contamos con uno de los 4 métodos avalados  que es la facturación POS
O más bien con dos de los 4 métodos por que la facturación por computador también 
es un método de facturación avalado más bien somos una combinación en poco tiempo 

Nuestro sistema también dispondra de la facturación electrónica.

Regresando con la facturación POS es una modalidad de bastante de uso a nivel nacional 
Empleado mayor por almacenes con alto volumen de transacciones ya que este sistema mejora el rendimiento del proceso en gran cantidad, también es muy utilizado por establecimientos comerciales medianos como las pequeñas y medianas PYMES.

 La facturación POS consiste en considerar factura el tiquete generado por la máquina registradora en el momento en que se hace el pago de un bien o servicio, partiendo de la base que dicho tiquete ha sido generado por medio de un software especializado aceptado por la Dirección de Impuestos y Aduanas Nacionales (DIAN).

Este tiquete debe cumplir con los siguientes requisitos:

    Nombre o razón social y número de identificación tributaria (NIT) del vendedor del bien o prestador del servicio.

    Número de la transacción, esto debido a que todas las facturas deben seguir un orden consecutivo de numeración, en el caso de las máquinas registradoras, puede contar con nominaciones alfanuméricas, sin que exceda cuatro caracteres.

    Fecha de la operación.

    Descripción del servicio o bien adquirido. 

    Valor total de la transacción.

Configuración de Resolución de Facturación
------------------------------------------
*Obligación
La resolución de facturación la expidió la Dian en la resolución 00055 de julio 21 de 2016, la cual reglamentó la obligación de solicitar autorización para facturar, por lo tanto esta es de obligatorio cumplimiento.

*Prefijo
Cuando el contribuyente tiene varios establecimientos de comercio, requiere identificar o separar las ventas según sus necesidades particulares, puede recurrir a los prefijos como mecanismo auxiliar para identificar las facturas y los ingresos.

*Sanciones 
a vimos que uno de los requisitos de la factura es contener la resolución de facturación, de modo que si no se tiene, o se gestiona pero no se imprime en la factura, estamos ante un hecho irregular relacionado con la facturación, y se impone la respectiva sanción por facturación.

Paso a Paso

**1) Boton :** :guilabel:`Nuevo`.

**2) Sucursal:**click en el campo se abre una lista desplegable con las surcursables diponibles elegi una (*Obligatorio).

**3) Nombre:** escriba un nombre para su consecutivo(*Obligatorio).

**4) Sufijo:** este no es (*Obligatorio) y en la mayoria los casos este numero impuesto a gusto por las organizacion el numero digitado aqui va impreso en la factura el orden este es antes del # de factura.

**5) Numero Inicial:** para completar este dato debemos de tener el documento Solicitud sobre numeracion de facturacion de la Dian en el campo 27. Desde el numero es el dato que debemols digitar aqui (*Obligatorio).

.. image:: images/dian_factura27.jpg

Tarifas por sucursales
----------------------
Con opción brindamos la posibilidad de gestionar diferencias en entre los precios para nuestras sucursales de negocio por su localidad o posición geográfica.

Paso a Paso

Crear Tarifa por Sucursal
++++++++++++++++++++++++++

**1) Nombre:** Digitar el nombre que va tener la taruifa por sucursal.

**2) activo:** (casilla de verificacion o checkbox) Marca la casilla para activar la tarifa por sucursal.

Productos
----------

**3) Producto:** Digita el nombre del producto o da click en la lupa para iniciar una busqueda avanzada por filtros.

**4) Precio Venta:** Es precio que va tener el producto al cliente final.

**1) Impuesto:** :guilabel:`Guardar`.

.. Hint:: https://erpv3.source-solutions.co/facturacion/tarifa-por-sucursal/list.




Creación de Clientes
--------------------
Aquí podemos actualizar o modificar nuestra base datos de nuestros clientes 
*tener en cuenta que antes de agregar un nuevo cliente debemos crear una persona.

pendiente de actualizar
************************
**1) Boton :** :guilabel:`Nuevo`.

**2) Sucursal:**click en el campo se abre una lista desplegable con las surcursables diponibles elegi una (*Obligatorio).

**3) Nombre:** escriba un nombre para su consecutivo(*Obligatorio).

**4) Sufijo:** este no es (*Obligatorio) y en la mayoria los casos este numero impuesto a gusto por las organizacion el numero digitado aqui va impreso en la factura el orden este es antes del # de factura.

**5) Numero Inicial:** para completar este dato debemos de tener el documento Solicitud sobre numeracion de facturacion de la Dian en el campo 27. Desde el numero es el dato que debemols digitar aqui (*Obligatorio).

.. Hint:: https://erpv3.source-solutions.co/facturacion/cliente/list.


Creación de Facturas
---------------------

**1) Boton :** :guilabel:`Nuevo`.

**2) Fecha:** click en el icono de calendario y elecciona la fecha que desee para crear su factura. 

**3) Sucursal:** click en el campo se despleiga una lista de las surcursales disponibles.

**4) Cliente:** en este campo puedes digitar el nombre o numero de cedula de un cliente o buscar con la lupa con filtros de busqueda avanzada.

**5) Vendedor:** es muy  similar al campo cliente, solo con escribir unas cuantas letras del nombre del vendedor o del numero de cedula te aparecera en la lista desplegable las sugerencias, tambien puedes buscar en la lupa con losb  filtros de busqueda avanzada.

**6) Fecha  de Nacimiento:** Si sabes la fecha de nacimiento del cliente la puedes seleccionar desde el icono de calendario, en caso de no disponer de la informacion la puedes dejar en blanco por que esta informacion no es obligatoria. 

**7) Nota:** Es opcional si deseas escribir una nota.

Total
-----

Productos
---------

**1) producto:**
Digita el nombre del producto

**2) Precio:**

**3) Impuesto:**

**4) Descripción:** este campo es opcional puedes o no escribir una descripcion.

**1) Boton :** :guilabel:`Guardar`.

.. Hint:: https://erpv3.source-solutions.co/facturacion/cliente/list.


