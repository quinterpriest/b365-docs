.. _pos:

POS
===

Un sistema POS de facturación o punto de venta en español, es un conjunto de herramientas de hardware y software, 
que principalmente le permiten a los negocios facturar sus ventas, facilitando también llevar el control de su flujo de caja, inventarios, proveedores, compras,  cuentas por cobrar y pagar, gastos y costos fijos, utilidades y pérdidas, entre otras funciones.

.. _pos_login:

Logueo
------

Para acceder al sistema POS, se debe ingresar el siguiente :term:`URL` dentro de su :term:`Navegador web` Google Chrome:
https://erpv3.source-solutions.co/pos
Nota: Para efectos de este tutorial se ingresará a la aplicación utilizando un entorno de pruebas o
“demostración”.

.. image:: images/pos/pantalla-login.png
  :width: 550
  :align: center
  :alt: Pos pantalla login

Una vez digitado el :term:`URL` correspondiente, se desplegará la página de inicio del sistema :term:`POS`, donde se
podrá ingresar el número de documento y contraseña asignados.
