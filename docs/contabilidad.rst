.. _contabilidad:

Modulo Contabilidad
=================== 

Sistema de control y registro de los gastos e ingresos y demás operaciones económicas que realiza una empresa o entidad.

Impuestos
---------

Descripcion
------------
Impuestos proporcionales: la cuota a pagar se calcula a través de un porcentaje fijo, como por ejemplo el IVA. No se tiene en cuenta la base imponible o la renta del individuo sujeto al impuesto.
Podemos crear aquí el IVA del 8 y 19% una cuantas variantes necesitemos para nuestros 
Productos.

Paso a Paso
------------
*1: Boton :guilabel:`Nuevo`.

*2: Tipo Impuesto_:

*3: Nombre_: Escribe el nombre de la retencion.

*4: Porcentaje_: digite el porcentaje que va tener la retencion ejemplo 8%, 16%, %6.

*5: Aplicable_:

*6: Activo_:(casilla de verificacion o checkbox) para tener activa la renentencion le das click en el cuadrito.


*7: Descripcion_::jiikj:: la descripcion es opcional.....) 

*8 por ultimo click en el boton :guilabel:`Guardar`.


Retenciones
-----------

Una retención, en fiscalidad, es una imposición​ de las autoridades tributarias sobre el contribuyente para detraer parte de sus ingresos como forma de cobro anticipado de uno o varios impuestos correspondientes al año fiscal.

Crear Retencion
---------------
:::Paso a ::Paso_::
*1: Boton :guilabel:`Nuevo`.

*2: Tipo retencion_: En tipo de hay solo dos opciones Retfte y Ritica.

*3: Nombre_: Escribe el nombre de la retencion.

*4: Porcentaje_: digite el porcentaje que va tener la retencion ejemplo 8%, 16%, %6.

*5: Aplicable_:

*6: Activo_:(casilla de verificacion o checkbox) para tener activa la renentencion le das click en el cuadrito.


*7: Descripcion_::jiikj:: la descripcion es opcional.....) 

*8 por ultimo click en el boton :guilabel:`Guardar`.

.. seealso::

    ` <https://erpv3.source-solutions.co/contabilidad/retencion/list>`_


Formas de Pago
--------------

Descripcion
------------
en esta parte podemos configurar, modificar o incluso crear nuevas formas o métodos de pago por defecto están configurados consignación, transferencia, Efectivo, tarjeta
Débito o crédito.


*1: Boton :guilabel:`Nuevo`.

*2 Nombre_: Digitar el nombre que va tener la forma de pago, Ejemplo efectivo, Cheque etc..

*3 Necesita_: (casilla de verificacion o checkbox) para tener activa la renentencion le das click en el cuadrito si pago necesita transferencia bancaria.

*4 Necesita Franquicia_:

*5 Necesita Banco_:

*8 por ultimo click en el boton :guilabel:`Guardar`.


.. seealso::

    ` <https://erpv3.source-solutions.co/contabilidad/retencion/list>`_

Busqueda de Forma de Pago
-------------------------

*1

*2

*3

*4

*5

*6

*7

*8


.. seealso::

    ` <https://erpv3.source-solutions.co/contabilidad/forma-pago/list>`_