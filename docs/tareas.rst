.. _tareas:

Tareas
======

You can also save the user configuration for further use, either download them
as a file or to the browser local storage. You can find both those options in
the :guilabel:`Settings` tab. The settings stored in browser local storage will
be automatically offered for loading upon your login to phpMyAdmin.
