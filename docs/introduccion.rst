.. _introduccion:

Introducción
============

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/VUYkNIJfN6o" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>


Business 365 Es un sistema de gestión empresarial que le ayudará a ser más productivo en su empresa, ahora tendrá siempre a disposición la información detallada y actualizada de las diferentes áreas de su negocio, como estados de inventarios, cuentas por cobrar, clientes, proveedores, facturas y pagos.

Es importante para nosotros poner a su disposicion la documentación de nuestra plataforma B365, en las siguientes pantallas prepárese para adquirir el conocimiento necesario para gestionar su empresa con B365, aprenderas desde iniciar sesión en la plataforma como recuperar una cuenta, búsquedas de registros, crear registros nuevos.

Le recordamos suscribirse a nuestro canal de youtube para enterarte de todas las actualizaciones de nuestros productos.

.. seealso:: https://www.youtube.com/channel/UCzmA3kpqwxcd0jxOyEIl-vg


.. rubric:: Notas

.. [#f1]

    *Terminos relacionados :term:`Login`, :term:`Contraseña`, :term:`Pestañas`, :term:`Autocompletar`, :term:`Lista Desplegable`, :term:`Filtros de búsqueda`, :term:`CSV`, :term:`Paginador`. 
