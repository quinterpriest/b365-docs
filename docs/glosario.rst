.. _glosario:

Glosario
========

.. glossary::

    Autocompletar
      Es una característica de B365 que muestra a los usuarios consultas útiles cuando empiezan a escribir. Por ejemplo, cuando un usuario escriba "ca" en un campo de autocompletar, aparecerán automáticamente consultas como "cali" o cartagena"

    
    Contraseña
      Una contraseña o clave es una forma de autentificación que utiliza información secreta para controlar el acceso hacia algún recurso.

    
    CSV
      Los archivos CSV (del inglés comma-separated values) son un tipo de documento en formato abierto sencillo para representar datos en forma de tabla, en las que las columnas se separan por comas (o punto y coma en donde la coma es el separador decimal y las filas por saltos de línea.

      .. seealso:: https://es.wikipedia.org/wiki/Valores_separados_por_comas    
    
    
    Dian
      La Dirección de Impuestos y Aduanas Nacionales es una unidad administrativa especial del estado colombiano.

      .. seealso:: URL Pagina Oficial https://www.dian.gov.co

    
    Inventario
      Los inventarios son bienes reales y concretos, es decir bienes muebles e inmuebles.

    
    Login
      En español ingresar o entrar es el proceso mediante el cual se controla el acceso individual a un sistema informático.


    Lista Desplegable
      Una lista desplegable es un elemento de la interfaz de usuario que le permite seleccionar de una lista opciones mutuamente excluyentes.

    
    Filtros de búsqueda
      Es una característica de B365 que facilita la búsqueda en las pantalla de consulta de información de los módulos.

    
    FAQ
      FAQ son las siglas de la expresión inglesa Frequently Asked Questions, que en español podemos traducir como ‘preguntas frecuentes’. Como tal, es una lista de las preguntas más frecuentes con sus respectivas respuestas sobre un tema en particular.      

    
    JPEG
      JPEG es la sigla correspondiente a Joint Photographic Experts Group, un comité de especialistas que ideó un estándar de codificación y compresión de imágenes digitales. JPEG también es la denominación del formato que identifica a este tipo de documentos y la extensión de los archivos en cuestión.

      .. seealso:: https://es.wikipedia.org/wiki/Joint_Photographic_Experts_Group


    Linux
      GNU/Linux (también conocido informalmente como Linux, y apocopado como Lignux para diferenciarlo de otros sistemas que usan tal núcleo)1​ es un sistema operativo libre tipo Unix POSIX; multiplataforma, multiusuario y multitarea.
      
      .. seealso:: https://es.wikipedia.org/wiki/GNU/Linux


    Mac OS
      Mac OS (del inglés Macintosh Operating System, en español Sistema Operativo de Macintosh) es el nombre del sistema operativo creado por Apple para su línea de computadoras Macintosh.
      
      .. seealso:: https://es.wikipedia.org/wiki/Mac_OS


    Módulo
      una porción de un programa de ordenador. De las varias tareas que debe realizar un programa para cumplir con su función u objetivos.


    Navegador web
      Un navegador web (en inglés, web browser) es un software, aplicación o programa que permite el acceso a la Web, interpretando la información de distintos tipos de archivos y sitios web para que estos puedan ser vistos.


    Pestaña
      Una pestaña, solapa o lengüeta es un elemento de la interfaz de un programa que permite cambiar rápidamente lo que se está viendo sin cambiar de ventana que se usa en un programa o menú.

      .. seealso:: https://es.wikipedia.org/wiki/Pesta%C3%B1a_(inform%C3%A1tica)


    PDF
      PDF (sigla del inglés Portable Document Format, «formato de documento portátil») es un formato de almacenamiento para documentos digitales independiente de plataformas de software o hardware.
      
      .. seealso:: https://es.wikipedia.org/wiki/PDF


    POS
      son las siglas en inglés de “Point of Sale” y se traduce al español como "Punto de Venta" 


    URL
      Es una secuencia de caracteres que se utiliza para nombrar y localizar recursos, documentos e imágenes en Internet. URL significa "Uniform Resource Locator", o bien, "Localizador Uniforme de Recursos".

      .. seealso:: https://es.wikipedia.org/wiki/Localizador_de_recursos_uniforme

    Windows
      Es el nombre de una familia de distribuciones de software para PC, teléfonos inteligentes, servidores y sistemas empotrados, desarrollados y vendidos por Microsoft y disponibles para múltiples arquitecturas, tales como x86, x86-64 y ARM.   
